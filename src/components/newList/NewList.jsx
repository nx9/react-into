import React from "react";
import "./newList.scss";
import { ReactComponent as Star } from "../../img/star.svg";
import EqualizerIcon from "@mui/icons-material/Equalizer";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";

const NewList = ({ newData }) => {
  return (
        <li className="posts">
          <img src={newData.image} alt="img" />
          <h1>{newData.name}</h1>
          <div className="stars">
            <div className="star">
              <Star />
              <Star />
              <Star />
              <Star />
              <Star />
            </div>
          </div>
          <div className="price">
            <p className="oldPrice">
              {newData.old_price ? <p>{newData.old_price} 000 сум</p> : "Скидок нет"}
            </p>
            <span className="currentPrice">От {newData.price} 000 сум/12 мес</span>
            <p className="as-debt">От 400 000 сум/12 мес</p>
          </div>
          <div className="controls">
            {newData.in_stock > 0 ? (
              <button className="btn">В корзину</button>
            ) : (
              <button className="btn-false">В корзину</button>
            )}
            <EqualizerIcon className="cont" />
            <FavoriteBorderIcon className="cont" />
          </div>
        </li>
  );
};

export default NewList;
