import './category.scss'

const Category = () => {
  return (
    <div className='category'>
        <ul>
            <li>
                <a href='/'>Акции и скидки</a>
            </li>
            <li>
                <a href='/'>Ноутбуки и компьютеры</a>
            </li>
            <li>
                <a href='/'>Смартфоны и гаджеты</a>
            </li>
            <li>
                <a href='/'>Телевизоры и аудио</a>
            </li>
            <li>
                <a href='/'>Красота и здоровье</a>
            </li>
            <li>
                <a href='/'>Техника для дома</a>
            </li>
        </ul>
    </div>
  )
}

export default Category