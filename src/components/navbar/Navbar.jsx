import "./navbar.scss";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import LeaderboardIcon from "@mui/icons-material/Leaderboard";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import SearchIcon from "@mui/icons-material/Search";
import {ReactComponent as Logo} from '../../img/MaxProff.svg';
// import Category from "../categories/Category";
// import NewList from "../newList/NewList";

const Navbar = () => {
  return (
    <div>
      <div className="navbar">
        <ul>
          <li>
            <span className="logo">
              <Logo />
            </span>
          </li>
          <li>
            <form>
              <input type="text" placeholder="Искать товары" />
              <button>
                <SearchIcon className="icon" />
              </button>
            </form>
          </li>
          <li>
            <div className="first">
              <FavoriteBorderIcon />
              <span>Избранное</span>
            </div>
            <div className="second">
              <LeaderboardIcon />
              <span>Сравнение</span>
            </div>
            <div className="third">
              <AddShoppingCartIcon />
              <span>Корзина</span>
            </div>
            <div className="fourth">
              <PersonOutlineIcon />
              <span>Войти</span>
            </div>
          </li>
        </ul>
      </div>
      <hr />
      
    </div>
  );
};

export default Navbar;
