import axios from "axios";
import { useEffect, useState } from "react";
import Category from "./components/categories/Category.jsx";
import Navbar from "./components/navbar/Navbar.jsx";
import "./App.scss";
import NewList from "./components/newList/NewList.jsx";
// import NewPosts from './components/newPosts/NewPosts.jsx';

function App() {
  const [data, setData] = useState([]);
  const [seeAll, setSeeAll] = useState(false);

  const filterData = () => {
    axios
      .get("https://624b0e2171e21eebbcec0e9d.mockapi.io/api/v1/products")
      .then((res) => setData(res.data))
      .catch((e) => console.log(e));
  };
  useEffect(() => {
    filterData();
  }, []);

  const allPost = data.map((item) => <NewList key={item.id} newData={item} data={data} />);
  const filteredPosts = data.filter(item => item.status === 'new').map(i => (<NewList key={i.id} newData={i} />))
  const limitedData = data.slice(0, 6).map((item) => <NewList key={item.id} newData={item} data={data} />)

  return (
    <div className="App">
      <Navbar />
      <Category />
      <div className="newPoroducts">
        <h2>Новинки</h2>
        <ul className="newProduct-list">
          {filteredPosts}
        </ul>
      </div>
      <div className="wrapper">
        <h1>Выбор покупателей</h1>
        <button onClick={() => setSeeAll(!seeAll)}>Смотреть все</button>
      </div>
      <div className="newList">
        <ul className="posts-list">
          {seeAll ? allPost : limitedData}
        </ul>
      </div>
      {/* {filteredPosts} */}
    </div>
  );
}

export default App;
